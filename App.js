import React, { Component } from 'react';
import MapView, { Marker } from 'react-native-maps';
import { Image, StyleSheet, Text, View, Dimensions, TouchableOpacity, SafeAreaView } from 'react-native';
import * as Location from 'expo-location';
import Notification from './Notification';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: null,
      notify: false,
      message: 'This is a notification!',
    }
  }


  //sets the notification flag
  toggleNotification = () => {
    this.setState({
      notify: !this.state.notify,
    });
  }

  async getMyLocation() {
    let location = await Location.getCurrentPositionAsync();
    this.setState({
      location,
      message: 'Changed to You',
    });
    this.toggleNotification();
  }
  async getPOI1Location() {
    let location = await Location.getCurrentPositionAsync();
    location.coords.latitude = '33.307146';
    location.coords.longitude = '-111.681177';
    this.setState({
      location,
      message: 'Changed to PO1',
    });
    this.toggleNotification();

  }
  async getPOI2Location() {
    let location = await Location.getCurrentPositionAsync();
    location.coords.latitude = '33.423204';
    location.coords.longitude = '-111.939612';
    this.setState({
      location,
      message: 'Changed to POI2',
    });
    this.toggleNotification();

  }

  async componentDidMount() {
    console.log(`consoleDidMount being called`);
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status === 'granted') {
      this.getMyLocation();
    }
  }

  renderMap() {
    return this.state.location ?
      <MapView
        style={styles.map}
        region={{
          latitude: this.state.location.coords.latitude,
          longitude: this.state.location.coords.longitude,
          latitudeDelta: 0.09,
          longitudeDelta: 0.04,
        }}
      >
        <Marker coordinate={{
          latitude: this.state.location.coords.latitude,
          longitude: this.state.location.coords.longitude
        }}>
          <Image source={require('./assets/you-are-here.png')} style={{ height: 35, width: 35 }} />
        </Marker>
      </MapView> : <Text style={{ color: "red" }}>Map Loading Please Wait...</Text>
  }

  render() {
    const notify = this.state.notify
      ? <Notification
        autoHide
        message={this.state.message}
        onClose={this.toggleNotification}
      />
      : null;
    return (
      <SafeAreaView style={styles.container} >
        {notify}
        <Text>Assignment 5 Gregory Stocker</Text>

        <TouchableOpacity onPress={() => {
          console.log(`Getting My Location`)
          this.getMyLocation();
        }
        } style={styles.button}><Text>You</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => {
          console.log(`Getting POI1's Location`)
          this.getPOI1Location();
        }
        } style={styles.button}><Text>POI1</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => {
          console.log(`Getting POI2's Location`)
          this.getPOI2Location();
        }
        } style={styles.button}><Text>POI2</Text>
        </TouchableOpacity>

        {this.renderMap()}
      </SafeAreaView >
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height / 2,
  }, button: {
    flex: 1,
    backgroundColor: '#9ED6BF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    flex: 1,
    width: "100%",
    backgroundColor: '#9ED6BF',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 5,
  },

});