import React, { Component } from 'react';
import {
  Animated,
  Easing,
  StyleSheet,
  Text,
} from 'react-native';
/**
 * Were defineing something simple.
 * It is a property that receives the message to display, and 2 callbacks to run some action on start and on closeing. 
 */
export default class Notification extends Component {

  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
    this.state = {
      height: -950,
    };

  }
  static defaultProps = {
    delay: 5000,
    onClose: () => { },
    onOpen: () => { },
  };


  componentDidMount() {
    this.startSlideIn();
  }

  getAnimation(value, autoHide) {
    const { delay } = this.props;
    return Animated.timing(
      this.animatedValue,
      {
        toValue: value,
        duration: 500,
        easing: Easing.cubic,
        delay: autoHide ? delay : 0,
        useNativeDriver: false,



      }
    );
  }
  //for sliding in, we calculate values between 0 and 1. once amimation os done we run onOpen.
  //If the autoHide is true when opOpen is called, we run slide-out to get rid of the component. 
  startSlideIn() {
    const { onOpen, autoHide } = this.props;

    this.animatedValue.setValue(0);
    this.getAnimation(1)
      .start(() => {
        onOpen();
        if (autoHide) {
          this.startSlideOut();
        }
      });
  }
  //slide out caluclates the reverse, values from 1 to 0. We send autoHide as a param to getAnimation. This delays the animation 
  //by delay amount of millisecs. After animation is done, we run onClose. 
  startSlideOut() {
    const { autoHide, onClose } = this.props;

    this.animatedValue.setValue(1);
    this.getAnimation(0, autoHide)
      .start(() => onClose());
  }

  onLayoutChange = (event) => {
    const { layout: { height } } = event.nativeEvent;
    if (this.state.height === -950) {
      this.setState({ height });
    }
  }

  render() {
    const { message } = this.props;
    const { height } = this.state;
    const top = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-height, 0],
      useNativeDriver: true,
    });

    return (
      <Animated.View
        onLayout={this.onLayoutChange}
        style={[
          styles.main,
          { top }
        ]}
      >
        <Text style={styles.text}>{message}</Text>
      </Animated.View>
    );
  }
}

function emptyFn() { }

const styles = StyleSheet.create({
  main: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    padding: 10,
    position: 'absolute',
    left: 0,
    right: 0,
  },
  text: {
    color: '#fff',
  },
});