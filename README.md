# Assignment 5

## authors
Gregory Stocker
Dan Ward- Author of React Native Cookbook

## Instructions

Take your completed Exercise 5 App and expand it by adding the following:

    Using the "Creating animated notifications" recipe from the book as a guide, create a notification window that will slide down from the top and then back up after a few seconds.
    When the User taps one of the buttons to change from one point on the map to another, this notification should appear.
    The notification should say "Changed to {You, POI 1, POI2}" depending on which the User has just selected.
    Record a video of your app running in either the Android Emulator or the iOS Simulator.
        Your video must show you cycling through multiple locations.
            This should mean the notification appears and then disappears after a time.
        If you are not comfortable showing your location when recording the video, then only show POI 1 and POI 2.
    Put your video in the folder with your App, and zip the entire folder.
    Submit the zip folder here in Canvas.
